import org.junit.Assert;

public class SumSquareDifference {

    public static void main(String[] args) {
        tests(10, 2640);
        tests(100, 25164150);
    }


    private static long sumSquareDifference(int maxNum) {
        int sumOfSquares = 0;
        int sum = 0;
        for (int i = 1; i<=maxNum; i++) {
            sumOfSquares = (int) (sumOfSquares + Math.pow(i, 2));
            sum += i;
        }
        int squareOfSum = (int) Math.pow(sum, 2);

        System.out.println("the difference between the sum of the squares of the first " + maxNum +
                " natural numbers and the square of the sum is: " + (squareOfSum-sumOfSquares));

        return squareOfSum - sumOfSquares;
    }

    private static void tests(int maxNum, int difference) {
        Assert.assertEquals(sumSquareDifference(maxNum), difference);
    }
}
